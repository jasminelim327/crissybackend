package com.crissy.config;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
@Configuration
class SwaggerConfig {
    @Bean
    public OpenAPI customSwaggerConfig() {
        Info info = new Info()
                .title("Crissy Backend API")
                .description("Internal API Crissy")
                .version("0.0.1");
        return new OpenAPI().info(info);
    }
}