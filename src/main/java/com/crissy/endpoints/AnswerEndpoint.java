package com.crissy.endpoints;

import com.crissy.models.Answer;
import com.crissy.models.Question;
import com.crissy.models.User;
import org.springframework.web.bind.annotation.*;

@RestController
public class AnswerEndpoint {
    @GetMapping("/api/v1/question/{questionID}/answers")
    //   get list of answer for the question
    public Answer getAnswer(
            @PathVariable String questionID
    ) {
        User answerer = new User("U000001", "Jasmine", "Goh", "jasminegoh@gmail.com", "jasminegoh", "female", "Student", false);
        Question question = new Question("Q000001", "What is the best approach of doing this?");
        return new Answer(answerer, question, "A000001");
    }
    @PostMapping("/api/v1/answer")
    public Answer createAnswer(
            @RequestBody Answer answer
    ) {
        return answer;
    }
}