package com.crissy.endpoints;

import com.crissy.models.Question;
import com.crissy.models.User;
import org.springframework.web.bind.annotation.*;

@RestController
public class QuestionEndpoint {
    @GetMapping("/api/v1/question/{questionID}")
    public Question getQuestion(
            @PathVariable String questionID
    ) {
        return new Question("Q000001", "What do you think about this?");
    }
    @PostMapping("/api/v1/question")
    public Question createQuestion(
            @RequestBody Question question
    ) {
        return question;
    }
}