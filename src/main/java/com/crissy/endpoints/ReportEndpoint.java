package com.crissy.endpoints;
import com.crissy.models.Report;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReportEndpoint {
    @GetMapping("/api/v1/question/{questionID}/reports")
    public Report getReportforQuestion(
            @PathVariable String questionID
    ) {
        return new Report("Racist");
    }
    @GetMapping("/api/v1/question/{questionID}/answer/{answerId}/reports")
    public Report getReportforAnswer(
            @PathVariable String questionID,
            @PathVariable String answerId) {
        return new Report("Racist");
    }
    @PostMapping("/api/v1/question/{questionID}")
    public Report createReportforQuestion(
            @RequestBody Report report,
            @PathVariable String questionID
    ) {
        return report;
    }
    @PostMapping("/api/v1/question/{questionID}/answers/{answerId}")
    public Report createReportforAnswer(
            @RequestBody Report report,
            @PathVariable String answerId,
            @PathVariable String questionID
    ) {
        return report;
    }
}
