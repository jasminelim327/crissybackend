package com.crissy.endpoints;

import com.crissy.models.User;
import org.apache.naming.factory.SendMailFactory;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserEndpoint {

    @GetMapping("/api/v1/user/{userId}")
    public User getUser(
            @PathVariable String userId
    ) {
        return new User(userId, "Jasmine", "Lim", "jasminelim327@gmail.com", "jasminelim327" , "female", "Student", false);
    }
    @PostMapping("/api/v1/user")
    public User createUser(
            @RequestBody User user
    ) {
        return user;
    }
}