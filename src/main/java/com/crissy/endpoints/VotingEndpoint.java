package com.crissy.endpoints;

import com.crissy.models.Answer;
import com.crissy.models.User;
import com.crissy.models.Voting;
import org.springframework.web.bind.annotation.*;
@RestController
public class VotingEndpoint {
    @GetMapping("/question/{questionId}/upvote")
    public Voting getQuestionUpvote(
            @PathVariable String questionId
    ) {
        return new Voting();
    }
    @GetMapping("/question/{questionId}/answer/{answerId}/upvote")
    public Voting getAnswerUpvote(
            @PathVariable String questionId,
            @PathVariable String answerId
    ) {
        return new Voting();
    }
    @PostMapping("/api/v1/{questionId}/answer/{answerId}")
    public Answer upvoteAnswer(
            @PathVariable String questionId,
            @PathVariable String answerId,
            @RequestBody Voting voting
    ) {
//        do we need to instantiate everytime we query, cannot reference?
//        User answerer = new User("U000002", "Alan", "Meow", "alanmeow@gmail.com", "alanmeow", "male", "Student", false)
//        Answer answer = new Answer(answerer, "What", "")
////        answer.upvote();
////        return answer;
        return null;
    }

}


