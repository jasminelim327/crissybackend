package com.crissy.models;
import java.util.ArrayList;
import java.util.List;

public class Answer {
//    private final User answerer;
    private final Question question;
    private static String answerID;
    private final User answerer;
    private Voting voting;
    private List<Report> reports;
    public Answer(User answerer, Question question, String answerId ) {
        this.answerer = answerer;
        this.question = question;
        this.voting = new Voting();
        this.reports = new ArrayList<>();
        answerID =answerId;
    }
        public String getUsername() {
        return answerer.getUsername();
    }
    public Question getQuestion() {
        return question;
    }
    public Voting getVoting() {
        return voting;
    }
    public void setVoting(Voting voting) {
        this.voting = voting;
    }
    public User getAnswerer() {
        return answerer;
    }
    public List<Report> getReports() {
        return reports;
    }
    public void setReports(List<Report> reports) {
        this.reports = reports;
    }
    public void addReport(Report report) {
        this.reports.add(report);
    }
    public static String getAnswerID() {
        return answerID;
    }
    public void setAnswerID(String answerID) {
        Answer.answerID = answerID;
    }
}