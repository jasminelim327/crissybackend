package com.crissy.models;
import java.util.ArrayList;
import java.util.List;

public class Question {
    private User questioner;
    private String questionID;
    private String question;
    private List<Answer> answers;
    private List<Report> reports;
    private Voting voting;

    public Question(String questionID, String question) {
//        this.questioner = questioner;
        this.questionID = questionID;
        this.question = question;
        this.answers = new ArrayList<>();
        this.reports = new ArrayList<>();
        this.voting = new Voting();
    }

    public User getQuestioner() {
        return questioner;
    }
    public void setQuestioner(User questioner) {
        this.questioner = questioner;
    }
    public String getQuestionID() {
        return questionID;
    }
    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }
    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }
    public List<Answer> getAnswers() {
        return answers;
    }
    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
    public void addAnswer(Answer answer) {
        this.answers.add(answer);
    }
    public List<Report> getReports() {
        return reports;
    }
    public void setReports(List<Report> reports) {
        this.reports = reports;
    }
    public void addReport(Report report) {
        this.reports.add(report);
    }
    public Voting getVoting() {
        return voting;
    }
    public void setVoting(Voting voting) {
        this.voting = voting;
    }
}
