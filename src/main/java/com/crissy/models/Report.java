package com.crissy.models;

public class Report {
    private User reportingUser;
    private String reportReason;
    public Report(String reportReason) {
//        this.reportingUser = reportingUser;
        this.reportReason = reportReason;
    }
    public User getReportingUser() {
        return reportingUser;
    }
    public void setReportingUser(User reportingUser) {
        this.reportingUser = reportingUser;
    }
    public String getReportReason() {
        return reportReason;
    }
    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }
}
