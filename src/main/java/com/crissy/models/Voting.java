package com.crissy.models;

public class Voting {
    private int upvote;
    private int downvote;
    public Voting() {
        this.upvote = 0;
        this.downvote = 0;
    }
    public int getUpvote() {
        return upvote;
    }
    public void setUpvote(int upvote) {
        this.upvote = upvote;
    }
    public int getDownvote() {
        return downvote;
    }
    public void setDownvote(int downvote) {
        this.downvote = downvote;
    }
    public void upvote() {
        upvote++;
    }
    public void downvote() {
        downvote++;
    }
}
